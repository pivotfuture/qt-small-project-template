# qt-small-project-template

#### 介绍
Qt小型项目模板。

#### 目录说明

- bin：存放编译生成的目标文件
- src/app：存放main.c，QApplication子类等应用程序级别的代码。
- src/config：存放配置文件类
- src/controller：存放业务控制器类源码（MV<u>C</u>）
- src/database：存放数据库访问类源码
- src/global：存放全局声明定义
- src/libs：存放公共库，三方库，公共库源码
  - src/libs/algorithm：用于存放算法模块，算法模块比较独立，所以作为程序库对待
- src/model：存放数据模型（<u>M</u>VC）
- src/network：存放网络交互类
- src/utils：存放工具类
- src/view：存放视图类（M<u>V</u>C）
- src/handler：handler用于处理单个功能点，这个功能点通常是程序上的，而不是业务上的，例如处理某个消息的收发，响应某个定时器。作为对比，src/controller中的业务控制器，通常处理的是业务功能点，controller更高层一些。
- src/include：用于存放公共头文件，数据结构，接口文件等
