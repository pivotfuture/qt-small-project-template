#include "MainController.h"

MainController::MainController(RootController *rootController, QObject *parent)
    : QObject(parent), m_rootController(rootController),
      m_mainWindow(new MainWindow)
{

}

MainWindow *MainController::getMainWindow()
{
    return m_mainWindow;
}
