/*******************************************************************************
** @brief        : 根控制器
** @author       : pivot
** @date         : 2024-03-17 14:11
*******************************************************************************/

#ifndef ROOTCONTROLLER_H
#define ROOTCONTROLLER_H

#include <QObject>
#include "MainController.h"

class RootController : public QObject
{
    Q_OBJECT
public:
    explicit RootController(QObject *parent = nullptr);

public:
    MainController m_mainController;
};

#endif // ROOTCONTROLLER_H
