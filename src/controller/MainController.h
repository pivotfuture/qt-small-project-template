/*******************************************************************************
** @brief        : 主控制器
** @author       : pivot
** @date         : 2024-03-17 13:26
*******************************************************************************/


#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QObject>
#include "MainWindow.h"

class RootController;

class MainController : public QObject
{
    Q_OBJECT
public:
    explicit MainController(RootController *rootController, QObject *parent = nullptr);

    /**
     * @brief getMainWindow
     * @return
     */
    MainWindow *getMainWindow();

public:
    RootController *m_rootController;

private:
    MainWindow *m_mainWindow;
};

#endif // MAINCONTROLLER_H
