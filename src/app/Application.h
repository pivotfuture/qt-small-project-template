﻿#ifndef APPLICATION_H
#define APPLICATION_H

#include <QApplication>

class Application : public QApplication
{
public:
    Application(int &argc, char **argv, int flags = ApplicationFlags);

private:
    void init();

    void loadTranslation();
    void loadStylesheet();
};

#endif // APPLICATION_H
