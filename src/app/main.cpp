﻿#include "MainWindow.h"
#include "RootController.h"
#include "Application.h"

#include <QApplication>
#include <QTranslator>
#include <QDebug>

int main(int argc, char *argv[])
{
    Application a(argc, argv);

    // 创建根控制器
    RootController rootController;

    // 显示主窗口
    rootController.m_mainController.getMainWindow()->show();

    return a.exec();
}
