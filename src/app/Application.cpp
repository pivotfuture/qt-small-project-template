﻿#include "Application.h"
#include <QTranslator>
#include <QDebug>

Application::Application(int &argc, char **argv, int flags) :
    QApplication(argc, argv, flags)
{
    init();
}

void Application::init()
{
    loadTranslation();
    loadStylesheet();
}

void Application::loadTranslation()
{
    // 加载翻译
    QTranslator translator;
    bool ok = translator.load("://translations/zh_cn.qm");
    if (ok)
    {
        qApp->installTranslator(&translator);
    }
    else
    {
        qDebug() << "load translation file error!";
    }
}

void Application::loadStylesheet()
{
}
