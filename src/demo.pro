QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#  Separate debug release output
CONFIG (debug,debug|release) {
    DESTDIR = $$PWD/../bin/debug
} else {
    DESTDIR = $$PWD/../bin/release
}


win32 {
    # using utf8 text coding for visual studio.
    QMAKE_CXXFLAGS += /utf-8
}

include(app/app.pri)
include(model/model.pri)
include(view/view.pri)
include(controller/controller.pri)
include(libs/libs.pri)
include(config/config.pri)
include(database/database.pri)
include(network/network.pri)
include(utils/utils.pri)
include(global/global.pri)
include(handler/handler.pri)
include(include/include.pri)
include(interfaces/interfaces.pri)

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# translations
TRANSLATIONS += $$PWD/resource/translations/zh_cn.ts

RESOURCES += \
    resource/resource.qrc
