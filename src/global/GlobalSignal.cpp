#include "GlobalSignal.h"

GlobalSignal::GlobalSignal(QObject *parent)
    : QObject{parent}
{}

GlobalSignal *GlobalSignal::instance()
{
    static GlobalSignal gsignal;
    return &gsignal;
}
