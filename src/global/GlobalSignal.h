#ifndef GLOBALSIGNAL_H
#define GLOBALSIGNAL_H

#include <QObject>

class GlobalSignal : public QObject
{
    Q_OBJECT
public:
    static GlobalSignal *instance();

private:
    explicit GlobalSignal(QObject *parent = nullptr);
};

#endif // GLOBALSIGNAL_H
